'use client'

// 判断手机还是电脑
export default function isMobile() {
	if(typeof Window === 'undefined') return false
	return /Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator!.userAgent)
}
