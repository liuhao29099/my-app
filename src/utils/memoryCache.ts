type CacheEntry<T> = {
	data: T
	timestamp: number
}

const cacheDuration = 30 * 60 * 1000 // 60 minutes in milliseconds

const memoryCache: Record<string, CacheEntry<any>> = {}

export const setCache = <T>(key: string, data: T) => {
	const entry: CacheEntry<T> = {
		data,
		timestamp: Date.now(),
	}
	memoryCache[key] = entry
}

export const getCache = <T>(key: string): T | null => {
	const entry = memoryCache[key]

	if (!entry) {
		return null
	}

	if (Date.now() - entry.timestamp > cacheDuration) {
		delete memoryCache[key]
		return null
	}

	return entry.data
}
