'use client'

import isMobileFn from '@/utils/isMobile'
import { useEffect, useState } from 'react'

export default function DownloadNotify() {
	const [isMobile, setIsMobile] = useState(false)
	useEffect(() => {
		if (isMobileFn()) {
			setIsMobile(true)
		}
	}, [])

	return isMobile ? (
		<p className="text-lg text-red-500">手机用户点击二维码即可进入</p>
	) : (
		<>
			<p className="text-lg text-red-500">(微信扫无效，请勿使用微信扫码)</p>
			<p className="text-lg text-red-500"> (用手机浏览器扫，下载安装马上可看！)</p>
		</>
	)
}
