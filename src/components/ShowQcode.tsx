'use client'
import qrcode from 'qrcode'
import Image from 'next/image'
import { useEffect, useState } from 'react'

export default  function ShowQcode(props: { url: string }) {
  const [base64Url,setBase64Url] = useState('')
 useEffect(()=>{
  async function fetchImg () {
    if(!props.url) return 
    const imgUrl = await qrcode.toDataURL(Buffer.from(props.url, 'base64').toString('utf8'))
    setBase64Url(imgUrl)
  }
  fetchImg()
 },[props.url])
	return (
		base64Url && <Image
			onClick={() => window.open(props.url)}
			src={base64Url}
			alt="qcode"
			width={200}
			height={200}
		/>
	)
}
