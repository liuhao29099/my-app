import pkg from '../../package.json'
import ShowQcode from '@/components/ShowQcode'
import DownloadNotify from '@/components/DownloadNotify'

// async function getData (): Promise<string> {
// 	try {
// 		const res = await fetch(
// 			'https://1231232123.xiannvgong.net/asdJUUD765as7asHs74564asSSD6',
// 			{
// 				next: { revalidate: 600 }
// 			}
// 		)
// 		if (!res.ok) {
// 			throw new Error('Failed to fetch data')
// 		}
// 		const { urlRes } = await res.json()
// 		const url = Buffer.from(urlRes, 'base64').toString('utf8')
// 		return url
// 	} catch (error) {
// 		return ''
// 	}
// }

export default async function Home () {
	// const res = await getData()
	const res = "https://js.dnbfsg.com/download/2586_0.html"

	return res ? (
		<div className="text-base sm:text-2xl text-center pt-20 text-black">
			<div>
				系统全面升级，
				<span className="text-red-600 font-bold">数据移至手机平台 </span>
			</div>
			<div className="mb-4">不再提供解压密码，请直接下载APP观看！ </div>
			{/* <p>为规避风险，请使用浏览器扫一扫</p>
    <p>（微信扫一扫无效，请使用UC或手机自带浏览器）</p> */}
			<p>本产品安全无毒，担心的新用户</p>
			<p className="mb-8">建议尝试用旧手机或者手机模拟器下载试用 </p>
			<p className="font-extrabold">在线观看、独家资源</p>
			<p className="font-extrabold">更新最快、资源最好</p>
			<div className="flex justify-center">
				<ShowQcode url={res || ''} />
			</div>
			<DownloadNotify />
			<p style={
				{
					display: 'none'
				}
			}>{
					pkg && pkg.version
				}</p>
		</div>
	) : null
}
export const runtime = 'experimental-edge'
