import './globals.css'
import type { Metadata } from 'next'

export const metadata: Metadata = {
	title: '公告',
	description: '最新通知',
}

export default function RootLayout({ children }: { children: React.ReactNode }) {
	return (
		<html lang="zh-cn">
			<head>
				<meta charSet="UTF-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1.0" />
				<title>公告</title>
			</head>
			<body>{children}</body>
		</html>
	)
}
